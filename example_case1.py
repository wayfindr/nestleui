from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from creds import *
import time


def sleep(t):
	print("Sleep %d sec." %t)
	time.sleep(t)

def notify():
	#как уведомлять будем
	pass


def login(driver):
	driver.get(url)
	assert "Photo Stream" in driver.title
	elem = driver.find_elements_by_link_text("Войти в систему")
	if len(elem) != 1:
		#Если обьект не найден либо найдено нескольно - шлем письмо
		notify()
	elem[0].click()
	sleep(10)
	assert "Обнаружение домашней области" in driver.title
	elem = driver.find_element_by_xpath('/html/body/div[2]/div[2]/div[1]/div[2]/div/form/div[1]/div[2]/div/span')
	elem.click()
	assert "Вход" in driver.title
	elem = driver.find_element_by_xpath('//*[@id="userNameInput"]')
	elem.send_keys(user)
	sleep(2)
	elem = driver.find_element_by_xpath('//*[@id="passwordInput"]')
	elem.send_keys(passwd)
	sleep(2)
	driver.find_element_by_xpath('//*[@id="submitButton"]').click()
	sleep(12)
	assert "Photo Stream" in driver.title
	return True

driver = webdriver.Firefox()
if not login(driver):
	print("Fail")
else:
	print("Pass")
	driver.close()